import { Socket } from 'ngx-socket-io';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'chemistry-relations';
  constructor(private _socket:Socket){
 
  }
  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this._socket.on("game-over", (data) => {
			if (data.status) {
        console.log("GAAMMMEEE OOVVVEEERRR");
        this.stopAllBots();
			}
		})
  }
  public stopAllBots():void{
    this._socket.emit("bot-pause-all")
    // this._changeShowOverlay()
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._socket.removeListener("bot-pause-all")
    this._socket.removeListener("game-over")
  }
}
