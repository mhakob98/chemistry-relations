import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { httpParams } from './com/annaniks/chemistry-relations/params/httpParams';
import { ApiService } from './com/annaniks/chemistry-relations/services/api.service';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SocketIoModule, SocketIoConfig } from 'ngx-socket-io';
import { ToastrModule } from 'ngx-toastr';
import { AuthGuard } from './com/annaniks/chemistry-relations/services/authguard.service';
const config: SocketIoConfig = { url: 'http://192.168.0.114:3000/', options: {transports: ['websocket']} };
// const config: SocketIoConfig = { url: 'relations.games:5000/', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule, 
    SocketIoModule.forRoot(config),
    ToastrModule.forRoot()
  ],
  providers: [
    CookieService,
    {
      provide:'BASE_URL',useValue:httpParams.baseUrl
    },
    ApiService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
