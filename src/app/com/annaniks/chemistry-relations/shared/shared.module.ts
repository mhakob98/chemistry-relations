import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TopActionBarComponent } from './top-action-bar/top-action-bar.component';
import { KeysPipe } from './pipes/Keys.pipe';
import { TranslateKeysPipe } from './pipes/translate.pipe';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { AppearDirective } from './directives/appear.directive';
import { NgSelectModule } from '@ng-select/ng-select';

@NgModule({
  declarations: [
  TopActionBarComponent,
  KeysPipe,
  TranslateKeysPipe,
  AppearDirective
  ],
  imports: [
    CommonModule ,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    ScrollToModule.forRoot(),
    NgSelectModule,
  ],
  providers: [
  ],
  exports:[
    FormsModule,
    ReactiveFormsModule,
    TopActionBarComponent,
    KeysPipe,
    TranslateKeysPipe,
    ScrollToModule,
    AppearDirective,
    NgSelectModule,
  ]
})
export class SharedModule { }
