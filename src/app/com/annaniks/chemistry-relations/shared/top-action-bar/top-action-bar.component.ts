import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'top-action-bar',
  templateUrl: './top-action-bar.component.html',
  styleUrls: ['./top-action-bar.component.scss']
})
export class TopActionBarComponent implements OnInit {
  @Input() label:string = "sdasd";
  @Input() showSaveButton:boolean = true;
  @Output() isSaved = new EventEmitter()
  constructor() { }

  ngOnInit() {
  }
  public handleSaveButtonTap():void{
    this.isSaved.emit(true)
  }
}
