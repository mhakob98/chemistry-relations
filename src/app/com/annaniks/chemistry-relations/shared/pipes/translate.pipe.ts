import { PipeTransform, Pipe } from '@angular/core';
import { TranslateKeys } from '../../models/models';

@Pipe({name: 'translate'})
export class TranslateKeysPipe implements PipeTransform {
  transform(value, args:Array<TranslateKeys>) : any {    
    return args.filter((element:TranslateKeys)=>element.key===value.toString())[0].value;
  }
}