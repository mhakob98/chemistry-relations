import { NgModule } from '@angular/core';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTableModule} from '@angular/material/table';
import {MatPaginatorModule} from '@angular/material/paginator';
import {DragDropModule} from '@angular/cdk/drag-drop';
@NgModule({
  imports: [
      MatDialogModule,
      MatTableModule,
      MatPaginatorModule,
      DragDropModule
  ],
  exports:[
      MatDialogModule,
      MatTableModule,
      MatPaginatorModule,
      DragDropModule
  ]
})
export class MaterialModule { }
