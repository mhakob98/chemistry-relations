import { Injectable } from '@angular/core';
import { MenuItem } from '../models/models';
@Injectable()
export class MenuItemsService{
    private _menuItems:Array<MenuItem> = [
        {
            label: "Кастомизация интерфейса",
            routerLink:"/interface-customization",
            expandable:false
        },
        {
            label: "Города",
            routerLink:"/cities",
            expandable:false
        },
        {
            label: "Настройка экранов",
            routerLink:"/screen-settings",
            expandable:false
        },
        {
            label: "Настройка основной игры",
            routerLink:"/configurations",
            expandable:true
        },
        {
            label: "Настройка тестовой игры",
            routerLink:"/test-configurations",
            expandable:true
        },
        {
            label: "Команды",
            routerLink:"/teams",
            expandable:false
        },
        {
            label: "Клиенты",
            routerLink:"/clients",
            expandable:false
        },
        {
            label: "Пульт управления игрой",
            routerLink:"/remote-control",
            expandable:false
        },
        {
            label: "Статистика",
            routerLink:"/statistics",
            expandable:false
        },
        {
            label: "Холодные звонки",
            routerLink:"/cold-calls",
            expandable:false
        },
        {
            label: "Логи",
            routerLink:"/logs",
            expandable:false
        },
        {
            label: "Боты",
            routerLink:"/bots",
            expandable:false
        },
        {
            label: "Выйти",
            routerLink:"",
            expandable:false,
            logout:true
        }
    ]
    constructor() {}
    public menuItems():Array<MenuItem>{
        return this._menuItems;
    }

}