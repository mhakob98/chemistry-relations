import { Injectable } from '@angular/core';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

@Injectable()
export class ScrollService {

  constructor(private _scrollToService: ScrollToService) { }
  private _activeElementId:string = "mainCoefficient"
  public triggerScrollTo(target:string) {
    this.activeElementId = target;
    const config: ScrollToConfigOptions = {
      target: target
    };
    this._scrollToService.scrollTo(config)
  }
  get activeElementId():string{
    return this._activeElementId;
  }
  set activeElementId(id:string) {
    this._activeElementId = id;
  }
}