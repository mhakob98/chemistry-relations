import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { RemoteControlView } from './remote-control-view';
import { RemoteControlRoutingModule } from './remote-control.routing.module';
import { RemoteControlGameComponent } from './remote-control-game/remote-control-game.component';
import { SupportAndCrysisComponent } from './support-and-crysis/support-and-crysis.component';
import { RoundsBackupComponent } from './rounds-backup/rounds-backup.component';
import { RemoteControlService } from './remote-control.service';
import { SetConfigsComponent } from './set-configs/set-configs.component';
import { ScreenControlComponent } from './screen-control/screen-control.component';
import { ActionsComponent } from './actions/actions.component';
import { AddEnergyToTeamModal } from '../../../modals/add-energy-to-team/add-energy-to-team-modal';
import { MaterialModule } from '../../../shared/material.module';
import { AddClientsGameTimeModal } from '../../../modals/add-clients-game-time/add-clients-game-time-modal';

@NgModule({
    declarations: [
       RemoteControlView,
       RemoteControlGameComponent,
       SupportAndCrysisComponent,
       RoundsBackupComponent,
       SetConfigsComponent,
       ScreenControlComponent,
       ActionsComponent,
       AddEnergyToTeamModal,
       AddClientsGameTimeModal
    ],
    imports: [
     CommonModule,
     SharedModule,
     RemoteControlRoutingModule,
     MaterialModule
    ],
    providers: [
        RemoteControlService
    ],
    entryComponents:[AddEnergyToTeamModal,AddClientsGameTimeModal]
})
export class RemoteControlModule {}