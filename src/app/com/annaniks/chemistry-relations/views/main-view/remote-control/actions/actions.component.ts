import { Socket } from 'ngx-socket-io';
import { ToastrService } from "ngx-toastr";
import { AddClientsGameTimeModal } from "./../../../../modals/add-clients-game-time/add-clients-game-time-modal";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from "@angular/material/dialog";
import { AddEnergyToTeamModal } from "../../../../modals/add-energy-to-team/add-energy-to-team-modal";

@Component({
  selector: "actions",
  templateUrl: "./actions.component.html",
  styleUrls: ["./actions.component.scss"]
})
export class ActionsComponent implements OnInit {
  constructor(
    private _dialog: MatDialog,
    private _toastrService: ToastrService,
    private _socket:Socket
  ) {}

  ngOnInit() {}
  public addEnergyToTeam(): void {
    const dialogRef = this._dialog.open(AddEnergyToTeamModal, {
      width: "485px"
    });
    dialogRef.afterClosed().subscribe(result => {});
  }
  public addClients(): void {
    const dialogRef = this._dialog.open(AddClientsGameTimeModal, {
      width: "485px"
    });
    dialogRef.afterClosed().subscribe(result => {
      result.added == false
        ? this._toastrService.success("", "Вы не можете добавить клиентов пока игра не начата", {
            positionClass: "toast-top-center"
          })
        : this._socket.emit("some-clients-was-added");
    });
  }
}
