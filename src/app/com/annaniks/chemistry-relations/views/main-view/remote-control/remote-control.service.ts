import { Injectable } from "@angular/core";
import { ApiService } from './../../../services/api.service';

@Injectable()
export class RemoteControlService {
    constructor(private _apiService: ApiService) { }
    public getRoundCount() {
        return this._apiService.get('games/play/data')
    }
    public getAllTeams() {
        return this._apiService.get(`group/filter/query`);
    }
    public addClients(count:string) {
        return this._apiService.post(`client/city/signup/${count}`,{},"response","text")
    }
}