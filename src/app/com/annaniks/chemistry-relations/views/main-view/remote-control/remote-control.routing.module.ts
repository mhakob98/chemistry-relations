import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RemoteControlView } from './remote-control-view';

let remoteControlViewRoutes: Routes = [
    {
        path: '', component: RemoteControlView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(remoteControlViewRoutes)],
    exports: [RouterModule]
})
export class RemoteControlRoutingModule { }