import { Component, OnInit, Input } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { ToastrService } from 'ngx-toastr';

const ACTIONS:Array<object> = [
  {
    title:"Поддержка №1",
    description:"Отмена санкций \n Рост дохода у средних клиентов на 50% \n У малых клиентов открываются новые направления бизнеса",
    key:"s1"
  },
  {
    title:"Поддержка №2",
    description:"Квартальные премии \n Компании с ростом более 30% за раунд получают 120 GSKcoins \n Компании получают 200 GSKcoins за каждый совместный проект",
    key:"s2"
  },
  {
    title:"Кризис",
    description:"Кризис \n У 50% малых клиентов доход снизился на 30% \n Кризис уничтожил 50% контрактов у компаний с уровнем активности 1",
    key:"c1"
  },
  {
    title:"Мировой кризис",
    description:"Мировой кризис \n У 30% клиентов снизился доход \n Кризис уничтожил 30% контрактов у компаний с уровнем активности 2 \n Кризис уничтожил все контракты у компаний с уровнем активности 1",
    key:"c2"
  },
  {
    title:"Премия за проактивность",
    description:"",
    key:"s3"
  },
]
@Component({
  selector: 'support-and-crysis',
  templateUrl: './support-and-crysis.component.html',
  styleUrls: ['./support-and-crysis.component.scss']
})
export class SupportAndCrysisComponent implements OnInit {
  private _actions:Array<object> = ACTIONS;
  private _isGameStarted:boolean = false;
  constructor(private _socket:Socket,private _toastr: ToastrService  ) { }

  ngOnInit() {
    this._getControlButtonsState();
    this._socket.on("support-one", (data) => {      
      console.log(data);
      this._eventSuccessMessage("Поддержка №1 выполнена")
    });
    this._socket.on("support-two", (data) => {
      console.log(data);
      this._eventSuccessMessage("Поддержка №2 выполнена")
    });
    this._socket.on("support-3-broadcast", (data) => {
      console.log(data);
      this._eventSuccessMessage("Премия за проактивность выдано")
    });
    this._socket.on("crizis", (data) => {
      console.log(data);
      this._eventSuccessMessage("Кризис выполнена")
    });
    this._socket.on("big-crizis", (data) => {
      console.log(data);
      this._eventSuccessMessage("Мировой кризис выполнена")
    });
  }
  get actions():Array<object>{
    return this._actions;
  }
  public processEvents(eventKey:string):void{
    console.log(eventKey);
    
    switch (eventKey) {
      case  's1' :
        this._socket.emit("support-one")
      break;
      case  's2' :      
        this._socket.emit("support-two")
      break;
      case  's3' :
        this._socket.emit("support-3-broadcast")
      break;
      case  'c1' :
        this._socket.emit("crizis")
      break;
      case  'c2' :
        this._socket.emit("big-crizis")
      break;
    }
  }
  private _eventSuccessMessage(successMessage:string):void{
    this._toastr.success('',successMessage,{
      positionClass:'toast-top-center'
    })
  }
  private _getControlButtonsState():void{  
    this._socket.emit('buttons-state');  
    this._socket.on('buttons-state',(state:any)=>{
      console.log(state.data);
      this._isGameStarted = true;
    })

  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._socket.removeListener('support-one')
    this._socket.removeListener('support-two')
    this._socket.removeListener('support-3-broadcast')
    this._socket.removeListener('crizis')
    this._socket.removeListener('big-crizis')
    this._socket.removeListener('buttons-state')
  }
}
