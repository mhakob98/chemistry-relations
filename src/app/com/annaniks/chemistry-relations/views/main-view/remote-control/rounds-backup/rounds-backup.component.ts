import { RemoteControlService } from "./../remote-control.service";
import { Component, OnInit } from "@angular/core";
import { Socket } from "ngx-socket-io";

@Component({
  selector: "rounds-backup",
  templateUrl: "./rounds-backup.component.html",
  styleUrls: ["./rounds-backup.component.scss"]
})
export class RoundsBackupComponent implements OnInit {
  private _roundCount: any;
  private _roundIndex: number;
  private _currentRound: number;
  private _showBackUpView: boolean = false;
  constructor(
    private _remoteControlService: RemoteControlService,
    private _socket: Socket
  ) {}

  ngOnInit() {
    this.getRoundCount();
    this._socket.on("restore-rounds", data => {
      console.log(data);
      this.getRoundCount()
    });
    this._socket.on("round-index", data => {
      this._roundIndex = data;
    });
  }

  public backUp(roundIndex): void {
    this._socket.emit("restore-rounds", { roundIndex });
  }
  private getRoundCount(): void {
    this._remoteControlService.getRoundCount().subscribe((roundCount: any) => {
      console.log(roundCount);

      this._roundCount = this._returnArrayFromNumber(
        roundCount[0].ROUNDS_COUNT
      );
      this._roundIndex = roundCount[0].roundIndex;
      this._showBackUpView = true;
    });
  }
  private _returnArrayFromNumber(length: number) {
    return new Array(length);
  }
  public isDisable(index):boolean{
    if(index > this._roundIndex){      
      return true
    }
    else {
      return false;
    }
  }
  get roundCount(): number {
    return this._roundCount;
  }
  get currentRound(): number {
    return this._currentRound;
  }
  get showBackUpView(): boolean {
    return this._showBackUpView;
  }
  ngOnDestroy(): void {
    this._socket.removeListener("restore-rounds");
    this._socket.removeAllListeners("round-index");
  }
}
