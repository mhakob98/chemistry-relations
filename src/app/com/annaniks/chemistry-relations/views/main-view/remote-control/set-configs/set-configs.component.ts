import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';

@Component({
  selector: 'set-configs',
  templateUrl: './set-configs.component.html',
  styleUrls: ['./set-configs.component.scss']
})
export class SetConfigsComponent implements OnInit {
  private _showLoadingOverlay:boolean = false;
  constructor(private _socket:Socket) { }

  ngOnInit() {
    this._socket.on("set-config",data=>{
      this._showLoadingOverlay = false;
    })
  }
  public setConfigs():void{
    this._showLoadingOverlay = true;
    this._socket.emit("set-config")
  }
  get showLoadingOverlay():boolean{
    return this._showLoadingOverlay;
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._socket.removeListener("set-config");
  }
}
