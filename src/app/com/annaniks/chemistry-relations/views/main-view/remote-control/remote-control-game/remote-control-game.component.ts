import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
const CONTROLKEYS:Array<object> = [
  {
    key:"start",
    value:"Старт",
    icon:"play_arrow"
  },
  {
    key:"pause",
    value:"Пауза",
    icon:"pause"
  },
  {
    key:"skip",
    value:"Пропустить таймер",
    icon:"skip_next"
  },
 
]
@Component({
  selector: 'remote-control-game',
  templateUrl: './remote-control-game.component.html',
  styleUrls: ['./remote-control-game.component.scss']
})
export class RemoteControlGameComponent implements OnInit {
  @Output()
  controlButtonToggled: EventEmitter<object> =  new EventEmitter<object>();
  @Input()
  controlButtonsState:object;
  @Input()
  isTestGame:boolean;
  private _controlKeys:Array<object> = CONTROLKEYS
  constructor() { }

  ngOnInit() {
 
  }
  public controlButtonClicked(controlButtonKey:string,isTest:boolean):void{
    this.controlButtonToggled.emit({key:controlButtonKey,isTest:isTest})
  }
  public checkIfButtonActive(controlButtonKey:string,isTest:boolean):boolean{
    if(this.controlButtonsState[controlButtonKey] == true){
      return false
    } else {
      return true
    }
  }
  get controlKeys():Array<object>{
    return this._controlKeys
  }
}
