import { Component, OnInit } from '@angular/core';
import { Socket } from 'ngx-socket-io';
const SCREENS = [
  {
    img:"assets/statistics/2.jpeg",
    label:"Рейтинг",
    eventName:'statistic-a'
  },
  {
    img:"assets/statistics/3.jpeg",
    label:"Занятость связей",
    eventName:'statistic-d'
  },
  {
    img:"assets/statistics/5.jpeg",
    label:"Выявленная потребность",
    eventName:'statistic-c'
  },
  {
    img:"assets/statistics.jpg",
    label:"Рост активов",
    eventName:'statistic-b'
  },
  {
    img:"assets/statistics/4.jpeg",
    label:"Уровень усиления связей",
    eventName:'statistic-e'
  },
  {
    img:"assets/statistics/6.jpg",
    label:"Главный экран",
    eventName:'show-main'
  }
]
@Component({
  selector: 'screen-control',
  templateUrl: './screen-control.component.html',
  styleUrls: ['./screen-control.component.scss']
})
export class ScreenControlComponent implements OnInit {
  private _screens = SCREENS
  private _showOverlay:boolean = false
  constructor(private _socket:Socket) { }

  ngOnInit() {
    this._socket.on('statistic-d',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse,"+++++++++");
    })
    this._socket.on('statistic-c',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse);
    })
    this._socket.on('statistic-e',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse);
    })
    this._socket.on('statistic-b',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse);
    })
    this._socket.on('statistic-a',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse);
    })
    this._socket.on('show-main',eventResponse =>{
      this._showOverlay = false;
      console.log(eventResponse);
    })
  }
  public showScreenInLive(eventName:string):void{
      this._showOverlay = true;
      console.log(eventName);
      this._socket.emit(eventName);
  }
  get screens(){
    return this._screens
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
  ngOnDestroy(): void {
    this._socket.removeListener('statistic-d')
    this._socket.removeListener('statistic-c')
    this._socket.removeListener('statistic-e')
    this._socket.removeListener('statistic-b')
    this._socket.removeListener('statistic-a')
    this._socket.removeListener('show-main')
  }
}
