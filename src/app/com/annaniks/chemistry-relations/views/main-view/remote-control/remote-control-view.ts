import { Observable } from "rxjs";
import { Component, OnInit } from "@angular/core";
import { Socket } from "ngx-socket-io";
import { RemoteControlService } from './remote-control.service';

@Component({
  selector: "app-remote-control",
  templateUrl: "./remote-control-view.html",
  styleUrls: ["./remote-control-view.scss"]
})
export class RemoteControlView implements OnInit {
  private _controlButtonsState: object;
  private _isGameStarted: boolean = false;
  private _isTestGameStarted: boolean = false;
  constructor(private socket: Socket,private _remoteControlService:RemoteControlService) {}

  ngOnInit() {
    this.checkIfGameStarted();
    this._getControlButtonsState();
    this.socket.on("pause-game-wait", gameState => {});
    this.socket.on("wait-game", time => {
      console.log(2);
    });
    this.socket.on("skip-game", gameState => {
      console.log(3);
    });
    this.socket.on("resume-timer-wait", gameState => {
      console.log(4);
    });
    this.socket.on("  ", data => {
      console.log(5);
      this._isGameStarted = false;
      this._isTestGameStarted = false;
    });
    this.socket.on("pause-game-test", gameState => {
      console.log(6);
    });
    this.socket.on("skip-test-game", gameState => {
      console.log(7);
    });
    this.socket.on("resume-timer-test", gameState => {
      console.log(8);
    });
    this.socket.on("pause-game-real", gamePaused => {
      console.log(9);
    });
    this.socket.on("resume-timer-real", gameResumed => {
      console.log(10);
    });
    this.socket.on("game-start", data => {
      data.status
        ? (this._isGameStarted = true)
        : (this._isGameStarted = false);
    });
    this.socket.on("game-start-test", data => {
      data.status
        ? (this._isTestGameStarted = true)
        : (this._isTestGameStarted = false);
    });
  }

  public handleButtonTogle(controlButton: object): void {
    if (!controlButton["isTest"]) {
      switch (controlButton["key"]) {
        case "start": {
          this._startGame();
          break;
        }
        
        case "pause": {
          console.log(this._isGameStarted,"9+-++++++++++++++++++++++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+");
          
          this._isGameStarted ? this.pauseRealGame() : this._stopGame();
          //  this._stopGame();
          break;
        }
        case "skip": {
          this._skipTimer();
          break;
        }
        case "resume": {
          this._isGameStarted ? this.resumeRealGame() : this._resumeGame();
          // this._resumeGame()
          break;
        }
        case "reset": {
          this._resetGame();
          break;
        }
      }
    } else {
      switch (controlButton["key"]) {
        case "start": {
          this._startTestGame();
          break;
        }
        case "pause": {
          this._isGameStarted ? this.pauseRealGame() : this._stopTestGame();
          // this._stopTestGame()
          break;
        }
        case "skip": {
          this._skipTestTimer();
          break;
        }
        case "resume": {
          this._isGameStarted
            ? this.resumeRealGame()
            : this._resumeTestGame();
          // this._resumeTestGame();
          break;
        }
        case "reset": {
          this._resetGameSavingGroup();
          break;
        }
      }
    }
  }

  get controlButtonsState(): object {
    return this._controlButtonsState;
  }

  private _getControlButtonsState(): void {
    this.socket.emit("buttons-state");
    this.socket.on("buttons-state", (state: any) => {
      this._controlButtonsState = state.data;
    });
  }

  private _startGame(): void {
    this.socket.emit("wait-game");
  }
  private _stopGame(): void {
    console.log("pause-game-wait");
    
    this.socket.emit("pause-game-wait");
  }
  private _skipTimer(): void {
    this.socket.emit("skip-game");
  }
  private _resumeGame(): void {
    this.socket.emit("resume-timer-wait");
  }
  private _resetGame(): void {
    this.socket.emit("reset-game", "reset-game");
  }
  private _startTestGame(): void {
    this.socket.emit("wait-game-test", true);
  }
  private _stopTestGame(): void {
    this.socket.emit("pause-game-test");
  }
  private _skipTestTimer(): void {
    this.socket.emit("skip-test-game");
  }
  private _resumeTestGame(): void {
    this.socket.emit("resume-timer-test");
  }
  private _resetGameSavingGroup(): void {
    this.socket.emit("reset-game-test", "");
  }
  public pauseRealGame(): void {
    this.socket.emit("pause-game-real", this._isGameStarted);
  }
  public resumeRealGame(): void {
    this.socket.emit("resume-timer-real", this._isGameStarted);
    console.log("pause-timer-wait");

  }
  private checkIfGameStarted(){
    this._remoteControlService.getRoundCount().subscribe(data=>{
        if(data.length > 0){
          this._isGameStarted = true;
        }  
        else{
          this._isGameStarted = false
        }
    })
  }
  ngOnDestroy(): void {
    this.socket.removeListener("buttons-state");
    this.socket.removeListener("pause-game-wait");
    this.socket.removeListener("wait-game");
    this.socket.removeListener("skip-game");
    this.socket.removeListener("resume-timer-wait");
    this.socket.removeListener("reset-game");
    this.socket.removeListener("pause-game-test");
    this.socket.removeListener("skip-test-game");
    this.socket.removeListener("resume-timer-test");
    this.socket.removeListener("pause-game-real");
    this.socket.removeListener("resume-timer-real");
    this.socket.removeListener("game-start");
    this.socket.removeListener("game-start-test");
  }
}
