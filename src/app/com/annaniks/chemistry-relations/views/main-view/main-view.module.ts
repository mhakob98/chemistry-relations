import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from './../../shared/shared.module';
import { SideNavigationMenuComponent } from './../../components/side-navigation-menu/side-navigation-menu.component';
import { MainViewRoutingModule } from './main-view.routing.module';
import { MainView } from './main-view';
import { ScrollService } from '../../services/scroll.service';
import { ScrollToService } from '@nicky-lenaers/ngx-scroll-to';

@NgModule({
    declarations: [
        SideNavigationMenuComponent,
        MainView,
    ],
    imports: [
     CommonModule,
     SharedModule,
     MainViewRoutingModule
    ],
    providers: [
        ScrollService,
        ScrollToService
    ],
    entryComponents:[]
})
export class MainViewModule { }