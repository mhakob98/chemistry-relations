import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'main-view',
  templateUrl: './main-view.html',
  styleUrls: ['./main-view.scss']
})
export class MainView implements OnInit {
  private _isMenuOpen:boolean = true;
  private _flagOfMenu:boolean = true;
  private _showOverlay:boolean = true;
  constructor() { }

  ngOnInit() {
    this._actionsWithOverlayAndMenu(window.innerWidth)
  }

  
  public toggleMenu():void{
    this._isMenuOpen =! this._isMenuOpen; 
    this._showOverlay =! this._showOverlay
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this._actionsWithOverlayAndMenu(event.target.innerWidth)
  }
  private _actionsWithOverlayAndMenu(screenSize:number):void{
    if(screenSize > 1024){
      this._isMenuOpen = false;
      this._flagOfMenu = true;
      this._showOverlay = true;
    } else {
      if(this._flagOfMenu){        
        this._isMenuOpen = true;
        this._flagOfMenu = false;
      }
    }
  }
  get isMenuOpen():boolean{
    return this._isMenuOpen;
  }
  get showOverlay():boolean{
    return this._showOverlay;
  }
}
