import { ConfigurationsView } from './configurations-view';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { ConfigurationsRoutingModule } from './configurations.routing.module';

@NgModule({
    declarations: [
       ConfigurationsView
    ],
    imports: [
     CommonModule,
     SharedModule,
     ConfigurationsRoutingModule
    ],
    providers: [
   
    ],
    entryComponents:[]
})
export class ConfigurationsModule {}