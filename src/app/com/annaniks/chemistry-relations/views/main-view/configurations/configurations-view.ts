import { Component, OnInit } from '@angular/core';
import { ConfigurationsService } from './configurations.service';
import { TranslateKeys } from '../../../models/models';
import { ScrollService } from '../../../services/scroll.service';
import { Router } from '@angular/router';
const KEYSTRANSLATIONS:Array<TranslateKeys> = [
  {
    key:"name",
    value:"Название"
  },
  {
    key:"channalsCount",
    value:"Количество связей"
  },
  {
    key:"energy",
    value:"Передаваемая энергия "
  },
  {
    key:"time",
    value:"Периодичность"
  }
 
  
]
const CLIENTKEYSTRANSLATIONS:Array<TranslateKeys> = [
   {
    key:"energy",
    value:"Премия за выявленную потребность"
  },
  {
    key:"time",
    value:"Время образования потребности (мин)"
  },
  {
    key:"0",
    value:"Коэффициенты первого канала"
  },
  {
    key:"1",
    value:"Коэффициенты второго канала"
  },
  {
    key:"2",
    value:"Коэффициенты третьего канала"
  },
  {
    key:"3",
    value:"Коэффициенты четвертого канала"
  },

]
@Component({
  selector: 'app-configurations',
  templateUrl: './configurations-view.html',
  styleUrls: ['./configurations-view.scss'],
  providers:[ConfigurationsService]
})
export class ConfigurationsView implements OnInit {
  private _showLoadingOverlay:boolean = false;
  private _keysTranslation:Array<TranslateKeys> = KEYSTRANSLATIONS;
  private _clientsKeysTranslations:Array<TranslateKeys> = CLIENTKEYSTRANSLATIONS;
  private _isTestConfigurations:boolean = false;
  public configs: any
  private showOverlay:boolean = true
  private smallClient = []
  private middleClient = []
  private bigClient = []
  private veryBigClient = []
  private _roundCount = [];
  constructor(private _configurationsService:ConfigurationsService,private _scrollService:ScrollService,private _router:Router) {
  
   }

  ngOnInit() {
    if(this._router.url == '/test-configurations'){
      this._isTestConfigurations = true;
    }
    this.isTestConfigurations ? this._getTestConfigurations() : this._getConfigurations();    
  }
  private _getConfigurations():void{
    this._showLoadingOverlay = true;
    this._configurationsService.getConfigurations().subscribe(configs=>{
      console.log(configs);
      this.configs = configs[0]
      this._setCliets()
      this._roundCount = this._returnArrayFromNumber(configs[0].ROUNDS_COUNT)
      this._showLoadingOverlay = false;
    });
  }
  private _getTestConfigurations():void{
    this._showLoadingOverlay = true;
    this._configurationsService.getTestConfigurations().subscribe(testConfigs=>{
      console.log(testConfigs);
      this.configs = testConfigs[0]
      this._setCliets()
      this._roundCount = this._returnArrayFromNumber(testConfigs[0].ROUNDS_COUNT)
      this._showLoadingOverlay = false;
    });
  }
  private _editConfigurations():void{
    this._showLoadingOverlay = true;
    this.isTestConfigurations ? this._configurationsService.editTestConfigurations(this.configs).subscribe(response=>{
        this._getTestConfigurations()
    }):
     this._configurationsService.editConfigurations(this.configs).subscribe(response=>{
      this._getConfigurations();
    })
    
  }
  public addRound(){
    ++this.configs.ROUNDS_COUNT;
    this._editConfigurations() 
  }
  public deleteRound(){
    --this.configs.ROUNDS_COUNT;
    this._editConfigurations() 
  }
  private _setCliets(){
    this.smallClient = Object.keys(this.configs.clients[0])
    this.middleClient = Object.keys(this.configs.clients[1])
    this.bigClient = Object.keys(this.configs.clients[2])
    this.veryBigClient = Object.keys(this.configs.clients[3])
  }
  private _returnArrayFromNumber(length:number){
    return new Array(length);
  }
  get showLoadingOverlay():boolean{
    return this._showLoadingOverlay;
  }
  get keysTranslation():Array<TranslateKeys>{
    return this._keysTranslation;
  }
  get clientsKeysTranslations():Array<TranslateKeys>{
    return this._clientsKeysTranslations;
  }
  get isTestConfigurations():boolean{
    return this._isTestConfigurations
  }
  get roundCount():any{
    return this._roundCount
  }
  
}
