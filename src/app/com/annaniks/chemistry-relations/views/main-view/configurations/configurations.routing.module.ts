import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfigurationsView } from './configurations-view';

let configurationsViewRoutes: Routes = [
    {
        path: '', component: ConfigurationsView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(configurationsViewRoutes)],
    exports: [RouterModule]
})
export class ConfigurationsRoutingModule { }