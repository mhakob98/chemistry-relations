import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { ApiService } from './../../../services/api.service';

@Injectable()
export class ConfigurationsService{
    constructor(private _apiService:ApiService){}

    public getConfigurations(){
        return this._apiService.get("games/data/true")
    }
    public getTestConfigurations(){
        return this._apiService.get("games/data/false")
    }
    public editConfigurations(newConfiguration) {
        console.log(newConfiguration);
        
        return this._apiService.put( "games/update/true", newConfiguration)
    }
    public editTestConfigurations(newConfiguration) {
        console.log(newConfiguration);
        
        return this._apiService.put( "games/update/false", newConfiguration)
    }
   
}
