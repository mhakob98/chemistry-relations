import { AddTeamModal } from './../../../modals/add-team-modal/add-team-modal';
import { TeamsService } from './teams.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-teams',
  templateUrl: './teams-view.html',
  styleUrls: ['./teams-view.scss']
})
export class TeamsView implements OnInit {
  private _allTeams:any
  constructor(private _teamsService:TeamsService,private _dialog: MatDialog) { }

  ngOnInit() {
    this._getAllTeams();
  }
  public addTeam(): void {
    const dialogRef = this._dialog.open(AddTeamModal, {
      width: '485px',
    });
    dialogRef.afterClosed().subscribe(result => {
      result.added == true ? this._getAllTeams():null 
    });
  }
  private _getAllTeams(filtered ? :any ):void{
    this._teamsService.getAllTeams(filtered).subscribe((teams:any)=>{
      this._allTeams = teams.result
      console.log(teams);
      
    })
  }
  private _deleteTeam(teamId:string):void{
    this._teamsService.deleteTeam(teamId).subscribe(response=>{
      this._getAllTeams();
    })
  }
  public deleteAllTeams():void{
    this._teamsService.deleteAllTeams().subscribe(response=>{
      this._getAllTeams()
    })
  }
  get allTeams():any{
    return this._allTeams;
  }
}
