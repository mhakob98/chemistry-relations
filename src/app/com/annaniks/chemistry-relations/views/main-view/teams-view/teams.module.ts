import { MaterialModule } from './../../../shared/material.module';
import { TeamsRoutingModule } from './teams.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { TeamsView } from './teams-view';
import { TeamsService } from './teams.service';
import { AddTeamModal } from '../../../modals/add-team-modal/add-team-modal';
import { TeamsFilterComponent } from './teams-filter/teams-filter.component';

@NgModule({
    declarations: [
       TeamsView,
       AddTeamModal,
       TeamsFilterComponent
    ],
    imports: [
     CommonModule,
     TeamsRoutingModule,
     SharedModule,
     MaterialModule
    ],
    providers: [
        TeamsService
    ],
    entryComponents:[AddTeamModal]
})
export class TeamsModule {}