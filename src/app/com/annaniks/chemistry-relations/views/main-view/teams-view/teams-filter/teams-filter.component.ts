import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TeamsService } from '../teams.service';
import { CityResponse } from './../../../../models/models';

@Component({
  selector: 'teams-filter',
  templateUrl: './teams-filter.component.html',
  styleUrls: ['./teams-filter.component.scss']
})
export class TeamsFilterComponent implements OnInit {
  @Input() teams:any;
  @Output() filteredTeams:EventEmitter<any> = new EventEmitter<any>();
  private _filterForm: FormGroup;
  private _allCities:CityResponse;
  constructor(private _fb: FormBuilder,private _teamsService:TeamsService) { }

  ngOnInit() {
    this._formBuilder();    
    this._getAllCities()
  }
  private _formBuilder():void {
    this._filterForm = this._fb.group({
      city:"",
      teamNumber:"",
      isConcurent:"",
      isRegistered:""
    });
    this._filterForm.valueChanges.subscribe((data)=>{
      this.filteredTeams.emit(data)
    })
  }
  private showAllTeams():void{
    this._filterForm.setValue({
      city:"",
      teamNumber:"",
      isConcurent:"",
      isRegistered:""
    })  
  }
  private _getAllCities():void{
    this._teamsService.getCities().subscribe((cities:CityResponse)=>{
      console.log(cities);
      this._allCities = cities
    })
  }
  get filterForm():FormGroup{
    return this._filterForm;
  }
  get allCities():CityResponse{
    return this._allCities;
  }
}
