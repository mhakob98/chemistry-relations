import { ApiService } from './../../../services/api.service';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { CityResponse } from '../../../models/models';
@Injectable()
export class TeamsService{
    constructor(private _apiService:ApiService){}
    public getAllTeams(filtered ? :any):Observable<any>{
        let filterQuery:string = ""
        if (filtered){
            filterQuery = `?cityId=${filtered.city}&code=${filtered.teamNumber}&isConcurent=${filtered.isConcurent}&status=${filtered.isRegistered}`            
        }
        console.log(filterQuery);
        
        return this._apiService.get(`group/filter/query${filterQuery}`);
    }
    public deleteTeam(id:string):Observable<any>{
        return this._apiService.delete(`group/delete/${id}`,"response","text")
    }
    public addTeam(team):Observable<any>{
        return this._apiService.post("group/signUp",team,"response","text")
    }
    public addSingleTeam(team):Observable<any>{
        let obj = {code:team.code,cityId:team.cityId,name:team.name,isConcurent:team.isConcurent}
        console.log(obj);
        
        return this._apiService.post("group/signUp/current",team,"response","text")
    }
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
    public deleteAllTeams():Observable<any>{
        return this._apiService.delete("group/delete","response","text")
    }
}