import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TeamsView } from './teams-view';

let teamsViewRoutes: Routes = [
    {
        path: '', component: TeamsView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(teamsViewRoutes)],
    exports: [RouterModule]
})
export class TeamsRoutingModule { }