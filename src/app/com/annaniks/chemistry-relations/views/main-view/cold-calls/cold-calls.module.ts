import { MaterialModule } from './../../../shared/material.module';
import { ColdCallsComponent } from './cold-calls.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { ColdCallsRoutingModule } from './cold-calls.routing.module';
import { ColdCallsService } from './cold-calls.service';
import { ColdCallModal } from '../../../modals/cold-call-modal/cold-call-modal';

@NgModule({
    declarations: [
       ColdCallsComponent,
       ColdCallModal
    ],
    imports: [
     CommonModule,
     SharedModule,
     ColdCallsRoutingModule,
     MaterialModule
    ],
    providers: [
        ColdCallsService
    ],
    entryComponents:[ColdCallModal]
})
export class ColdCallsModule {}