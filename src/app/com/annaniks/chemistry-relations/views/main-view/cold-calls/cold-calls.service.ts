import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './../../../services/api.service';
@Injectable()
export class ColdCallsService{
    constructor(private _apiService:ApiService){}
    // public generateNumbers(countAndEnergy):Observable<any>{
    //     console.log(countAndEnergy);
        
    //     return this._apiService.put( "games/update/true",countAndEnergy,"response","text")
    // }
    // public generateNumbersTest(countAndEnergy):Observable<any>{
    //     console.log(countAndEnergy);
        
    //     return this._apiService.put( "games/update/false",countAndEnergy,"response","text")
    // }
    public generateNumbers(countAndEnergy):Observable<any>{
        console.log(countAndEnergy);
        
        return this._apiService.post( "games/coldCalls",countAndEnergy,"response","text")
    }
    public deleteNumbers():Observable<any>{
        return this._apiService.delete("games/coldCalls","response","text")
    }
    public getNumbers():Observable<any>{
        return this._apiService.get("games/coldCalls")
    }
}