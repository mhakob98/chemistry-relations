import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ColdCallsComponent } from './cold-calls.component';

let coldCallsRoutes: Routes = [
    {
        path: '', component: ColdCallsComponent, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(coldCallsRoutes)],
    exports: [RouterModule]
})
export class ColdCallsRoutingModule { }