import { ColdCallModal } from './../../../modals/cold-call-modal/cold-call-modal';
import { Component, OnInit } from "@angular/core";
import { Socket } from "ngx-socket-io";
import { ColdCallsService } from "./cold-calls.service";
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: "app-cold-calls",
  templateUrl: "./cold-calls.component.html",
  styleUrls: ["./cold-calls.component.scss"]
})
export class ColdCallsComponent implements OnInit {
  private _generatedNumber: number;
  private _usedNumber: number;
  private _showLoadingOverlay: boolean = false;
  constructor(
    private _socket: Socket,
    private _coldCallsService: ColdCallsService,
    private _dialog: MatDialog
  ) {}

  ngOnInit() {
    this._getNumbers();
  }
  public openNumbersModal(): void {
    const dialogRef = this._dialog.open(ColdCallModal, {
      width: '485px',
    });
    dialogRef.afterClosed().subscribe(countAndEnergy => {   
      this.generateNumbers(countAndEnergy);
    });
  }
  public generateNumbers(countAndEnergy): void {
    this._showLoadingOverlay = true;
    this._coldCallsService.generateNumbers(countAndEnergy).subscribe(data=>{
      this._showLoadingOverlay = false;
      this._getNumbers();
    })
  }
  public deleteAllNumber(): void {
    this._showLoadingOverlay = true;
    this._coldCallsService.deleteNumbers().subscribe(data => {
      this._showLoadingOverlay = false;
      this._getNumbers()
    });
  }
  private _getNumbers(): void {
    this._coldCallsService.getNumbers().subscribe(data => {
      console.log(data);
      this._generatedNumber = data.all;
      this._usedNumber = data.countUsed;
      this._showLoadingOverlay = false;
    });
  }
  get generatedNumber(): number {
    return this._generatedNumber;
  }
  get usedNumber(): number {
    return this._usedNumber;
  }
  get showLoadingOverlay(): boolean {
    return this._showLoadingOverlay;
  }
}
