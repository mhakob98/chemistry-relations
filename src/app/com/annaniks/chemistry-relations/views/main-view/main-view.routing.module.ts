import { MainView } from './main-view';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let mainViewRoutes: Routes = [
    {
        path: '', component: MainView, children: [
            { path: '',redirectTo:"interface-customization",pathMatch:"full"},
            { path: 'interface-customization',loadChildren:'src/app/com/annaniks/chemistry-relations/views/main-view/interface-customization/interface-customization.module#InterfaceCustomizationModule' },
            { path: 'cities', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/cities-view/cities-view.module#CitiesViewModule' },
            { path: 'screen-settings', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/screen-settings/screen-settings.module#ScreenSettingsModule' },
            { path: 'configurations', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/configurations/configurations.module#ConfigurationsModule' },
            { path: 'test-configurations', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/configurations/configurations.module#ConfigurationsModule' },
            { path: 'remote-control', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/remote-control/remote-control.module#RemoteControlModule' },
            { path: 'teams', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/teams-view/teams.module#TeamsModule' },
            { path: 'clients', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/clients/clients.module#ClientsModule' },
            { path: 'statistics', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/statistic/statistic.module#StatisticModule' },
            { path: 'cold-calls', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/cold-calls/cold-calls.module#ColdCallsModule' },
            { path: 'logs', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/logs/logs.module#LogsModule' },
            { path: 'bots', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/bots/bots.module#BotsModule' }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(mainViewRoutes)],
    exports: [RouterModule]
})
export class MainViewRoutingModule { }