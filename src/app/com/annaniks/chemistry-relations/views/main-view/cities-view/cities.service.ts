import { ApiService } from './../../../services/api.service';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { CityResponse } from '../../../models/models';

@Injectable()
export class CitiesService{
    constructor(private _apiService:ApiService){}

    public addCity(cityName:string):Observable<CityResponse>{
       return this._apiService.post("/city",{name:cityName})
    }
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
    public removeCity(id:string):Observable<CityResponse>{        
        return this._apiService.delete(`/city/${id}`);
    }

}