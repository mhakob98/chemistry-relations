import { AddCityModal } from './../../../modals/add-city-modal/add-city-modal';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CitiesViewRoutingModule } from './cities-view-routing.module';
import { CitiesView } from './cities-view';
import { SharedModule } from '../../../shared/shared.module';
import { CitiesActionsComponent } from './cities-actions/cities-actions.component';
import { MaterialModule } from '../../../shared/material.module';
import { CitiesService } from './cities.service';

@NgModule({
    declarations: [
        CitiesView,
        CitiesActionsComponent,
        AddCityModal
    ],
    imports: [
     CommonModule,
     CitiesViewRoutingModule,
     SharedModule,
     MaterialModule
    ],
    providers: [
        CitiesService
    ],
    entryComponents:[AddCityModal]
})
export class CitiesViewModule {}