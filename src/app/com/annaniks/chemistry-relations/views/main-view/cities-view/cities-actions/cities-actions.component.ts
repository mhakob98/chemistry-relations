import { CityResponse } from './../../../../models/models';
import { CitiesService } from './../cities.service';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AddCityModal } from './../../../../modals/add-city-modal/add-city-modal';

@Component({
  selector: 'cities-actions',
  templateUrl: './cities-actions.component.html',
  styleUrls: ['./cities-actions.component.scss']
})
export class CitiesActionsComponent implements OnInit {
  private _allCities:CityResponse;
  constructor(private _dialog: MatDialog,private _citiesService:CitiesService) {}

  ngOnInit() {
    this._getCities();
  }
  public addCity(): void {
    const dialogRef = this._dialog.open(AddCityModal, {
      width: '485px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
      result.added == true ? this._getCities():null 
    });
  }
  private _getCities():void{
    this._citiesService.getCities().subscribe((cities:CityResponse)=>{
      if(cities.message == "ok"){
        this._allCities = cities  
      }
    })
  }
  private _removeCity(id:string):void{
    this._citiesService.removeCity(id).subscribe((response:CityResponse)=>{
      this._getCities();
    })
  }
  get allCities():CityResponse{
    return this._allCities
  }
}
