import { CitiesView } from './cities-view';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let citiesViewRoutes: Routes = [
    {
        path: '', component: CitiesView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(citiesViewRoutes)],
    exports: [RouterModule]
})
export class CitiesViewRoutingModule { }