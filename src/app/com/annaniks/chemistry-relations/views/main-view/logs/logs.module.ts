import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { LogsComponent } from './logs.component';
import { LogsRoutingMoudule } from './logs.routing.module';
import { LogsService } from './logs.service';
import { MaterialModule } from '../../../shared/material.module';


@NgModule({
    declarations: [
        LogsComponent
    ],
    imports: [
     CommonModule,
     SharedModule,
     LogsRoutingMoudule,
     MaterialModule
    ],
    providers: [
        LogsService
    ],
    entryComponents:[]
})
export class LogsModule {}