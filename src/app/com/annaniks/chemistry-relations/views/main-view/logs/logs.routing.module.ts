import { LogsComponent } from './logs.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let logsRoutes: Routes = [
    {
        path: '', component: LogsComponent, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(logsRoutes)],
    exports: [RouterModule]
})
export class LogsRoutingMoudule { }