import { ApiService } from './../../../services/api.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CityResponse } from '../../../models/models';

@Injectable()
export class LogsService{
    constructor(private _apiService:ApiService){}
    public getHistoriesCount(filterForm):Observable<any>{
        return this._apiService.get("group/log/length/query?userName=" + filterForm.userName + "&groupName=" + filterForm.groupName + "&operation=" + filterForm.operation + "&by=" + filterForm.by + "&cityId=" + filterForm.city )
    }
    public getHistoriesByPaginate(filterForm,pageIndex:number,pageSize:number):Observable<any>{
        console.log("group/log/" + pageIndex + "/"+ pageSize + "/query?userName=" + filterForm.userName + "&groupName=" + filterForm.groupName + "&operation=" + filterForm.operation + "&by=" + filterForm.by + "&cityId=" + filterForm.city);

        return this._apiService.get( "group/log/" + pageIndex + "/"+ pageSize + "/query?userName=" + filterForm.userName + "&groupName=" + filterForm.groupName + "&operation=" + filterForm.operation + "&by=" + filterForm.by + "&cityId=" + filterForm.city + "&isConcurent=" + filterForm.colors )

    }
    public getAllTeams(filtered ? :any):Observable<any>{
        return this._apiService.get(`group/filter/query`);
    }
    public getSettings():Observable<any>{
        return this._apiService.get("config/data");
    }
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
}