import { CityResponse } from './../../../models/models';
import { LogsService } from "./logs.service";
import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder } from "@angular/forms";
import { ApiService } from "../../../services/api.service";

@Component({
  selector: "app-logs",
  templateUrl: "./logs.component.html",
  styleUrls: ["./logs.component.scss"]
})
export class LogsComponent implements OnInit {
  private _showLoadingOverlay: boolean = false;
  private _filterForm: FormGroup;
  private _configs: any;
    private _historiesCount:number;

  private groups: Array<any>;
  private _dataSource: any;
  private _allCities:CityResponse;
  displayedColumns: string[] = [
    "groupName",
    "clientName",
    "userName",
    "energy",
    "operation",
    "time"
  ];

  constructor(
    private _apiService: ApiService,
    private fb: FormBuilder,
    private _logsService: LogsService
  ) {}

  ngOnInit(): void {
    this._getAllCities();
    this._getConfigs();
    this._getGroups();
    this._formBuilder();
    this._getHistoriesCount(this._filterForm);
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
  }
  private _paginatorChanged(event): void {
    this._getHistoriesByPaginate(+event.pageIndex, event.pageSize);
  }
  private _formBuilder(): void {
    this._filterForm = this.fb.group({
      operation: "",
      groupName: "",
      colors:"",
      by: "",
      userName: "",
      city:""
    });
    this._filterForm.valueChanges.subscribe(data => {
      console.log(data);
      
      this._getHistoriesCount(this._filterForm);
    });
  }
  private _showAllHistories(): void {
    this._filterForm.setValue({
      operation: "",
      groupName: "",
      colors:"",
      by: "",
      userName: "",
      city:""
    });
  }
  private _getConfigs(): void {
    this._logsService.getSettings().subscribe((configs: any) => {
      this._configs = configs[0]
      console.log(this._configs);
      
    });
  }
  private _getGroups(): void {
    this._logsService.getAllTeams().subscribe((groups: any) => {
      this.groups = groups;
      console.log(this.groups);
      
    });
  }
  private _getHistoriesCount(form): void {
    this._logsService.getHistoriesCount(form.value).subscribe((count: any) => {
      this._getHistoriesByPaginate(0, 10);
      console.log(count);
    });
  }
  private _getHistoriesByPaginate(pageIndex: number, pageSize: number): void {
    this._logsService
      .getHistoriesByPaginate(this._filterForm.value, pageIndex, pageSize)
      .subscribe(logs => {
        this._dataSource = logs;
        console.log(this._dataSource);
      });
  }
  private _getAllCities():void{
    this._logsService.getCities().subscribe((allCities:CityResponse)=>{
      this._allCities = allCities;
    })
  }
  get showLoadingOverlay(): boolean {
    return this._showLoadingOverlay;
  }
  get allCities():CityResponse{
    return this._allCities;
  }
}
