import { Observable } from 'rxjs';
import { ApiService } from './../../../services/api.service';
import { Injectable } from "@angular/core";

@Injectable()
export class InterfaceCustomizationService{
    
    constructor(private _apiService:ApiService){}

    public getSettings():Observable<any>{
        return this._apiService.get("config/data");
    }

    public uploadImage(formData:FormData,key:string):Observable<any>{
        return this._apiService.postFormData(`config/${key}/image`,formData)
    }

    public updateSettings(settings:any):Observable<any>{
        return this._apiService.put("config/update",settings)
    }

    public addField(formOfField:any):Observable<any>{
        console.log(formOfField);
        
        return this._apiService.post(`text/config/${formOfField.groupOfFields}`,{key:formOfField.key,ruKey:formOfField.ruKey,isImage:formOfField.isImage})
    }

    public getAllFieldsOfGroup(groupName:any):Observable<any>{
        return this._apiService.get(`text/config/${groupName}`)
    }

    public updateFieldsOfGroup(allFieldOfGroup:any,key:string):Observable<any>{
        return this._apiService.put(`text/config/${key}`,{data:allFieldOfGroup})
    }

    public uploadImageField(formData:FormData,group:string,id:string):Observable<any>{
        return this._apiService.postFormData(`config/${group}/image/${id}`,formData,"response","text")
    }
}