import { AddFieldModal } from './../../../modals/add-field-modal/add-field-modal';
import { InterfaceCustomizationService } from "./interface-customization.service";
import { Component, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material/dialog';
const INTERFACETYPES: Array<object> = [
  {
    key: "texts",
    value: "Тексты"
  },
  {
    key: "colors",
    value: "Цвета"
  }
];
const REGISTRATIONFIELDS: Array<object> = [
  {
    key: "mainBackground",
    value: "Основной фон"
  },
  {
    key: "fieldHeader",
    value: "Заголовок поля"
  },
  {
    key: "fieldBackground",
    value: "Фон поля"
  },
  {
    key: "buttonBackground",
    value: "Фон кнопки"
  },
  {
    key: "textField",
    value: "Текст в поле"
  },
  {
    key: "buttonText",
    value: "Текст в кнопке"
  }
];
@Component({
  templateUrl: "./interface-customization-view.html",
  styleUrls: ["./interface-customization-view.scss"]
})
export class InterfaceCustomizationView implements OnInit {
  private _interfaceTypes: Array<object> = INTERFACETYPES;
  private _activeType: object;
  private _settings: any;
  private _showLoadingOverlay: boolean = false;
  private _registrationFields: Array<object> = REGISTRATIONFIELDS;
  constructor(private _icService: InterfaceCustomizationService,private _dialog: MatDialog) {}

  ngOnInit() {
    this._activeType = this._interfaceTypes[1];
    this._getSettings();
  }
  public setActiveType(type: object): void {
    this._activeType = type;
  }
  public handleImageUpload(): void {
    this._getSettings();
  }
  private _updateSettings() {
    this._showLoadingOverlay = true;
    this._icService
      .updateSettings(this._settings)
      .subscribe((settings: any) => {
        this._getSettings();
        console.log(settings);
      });
  }
  private _getSettings(): void {
    this._icService.getSettings().subscribe(settings => {
      this._settings = settings[0];
      this._showLoadingOverlay = false;
      console.log(this.settings);
    });
  }
  public addField(): void {
    const dialogRef = this._dialog.open(AddFieldModal, {
      width: "485px"
    });
    dialogRef.afterClosed().subscribe(result => {
      // result.added == true ? this._getCities() : null;
    });
  }
  get interfaceTypes(): Array<object> {
    return this._interfaceTypes;
  }
  get activeType(): object {
    return this._activeType;
  }
  get registrationFields(): Array<object> {
    return this._registrationFields;
  }
  get settings(): any {
    return this._settings;
  }
  get showLoadingOverlay(): boolean {
    return this._showLoadingOverlay;
  }
}
