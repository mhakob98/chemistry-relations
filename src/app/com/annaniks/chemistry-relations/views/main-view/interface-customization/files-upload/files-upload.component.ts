import { InterfaceCustomizationService } from './../interface-customization.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { ImageUpload } from '../../../../models/models';
import { httpParams } from './../../../../params/httpParams';
const UPLOADLABELS:Array<ImageUpload> = [
  {
    label:"Логотип игры",
    key:"gameLogo"
  },
  {
    label:"Иконка жизни",
    key:"lifeIcon"
  },
  {
    label:"Иконка раундов",
    key:"iconRounds"
  },
  {
    label:"Иконка энергии",
    key:"energyIcon"
  },
  {
    label:"Иконка активности",
    key:"activityIcon"
  },
  {
    label:"Иконка списка",
    key:"listIcon"
  },
  {
    label:"Иконка рейтингов",
    key:"ratingIcon"
  },
  {
    label:"Иконка истории",
    key:"historyIcon"
  },
  {
    label:"Иконка правил игры",
    key:"gameRulesIcon"
  },
  {
    label:"Иконка связей",
    key:"linkIcon"
  },
  {
    label:"Иконка таймера",
    key:"timerIcon"
  },
  {
    label:"Иконка усиления связи",
    key:"communicationGainIcon"
  },
  {
    label:"Иконка предупреждения",
    key:"warningIcon"
  },
  {
    label:"Иконка совместного проекта",
    key:"jointProjectIcon"
  },
]
@Component({
  selector: 'files-upload',
  templateUrl: './files-upload.component.html',
  styleUrls: ['./files-upload.component.scss']
})
export class FilesUploadComponent implements OnInit {
  @Output() imageWasUplaoded:EventEmitter<void> = new EventEmitter<void>();
  @Input() settings:any;
  private _uploadLabels:Array<ImageUpload> = UPLOADLABELS;
  private _baseUrl:string = httpParams.photoBaseUrl;
  constructor(private _icService:InterfaceCustomizationService) { }

  ngOnInit() {
  }

  public uploadImage(event,key:string):void {    
    let formData = new FormData();
    if (event && event.target) {
      let fileList: FileList = event.target.files;
      if (fileList.length > 0) {        
        let file: File = fileList[0];
        formData.append('file', file, file.name);
      }
    }
    this._upload(formData,key)
  }
  get uploadLabels():Array<ImageUpload>{
    return this._uploadLabels;
  }

  get baseUrl():string{
    return this._baseUrl
  }
  private _upload(formData:FormData,key:string):void{
    this._icService.uploadImage(formData,key).subscribe(response=>{
      this.imageWasUplaoded.emit()
      console.log(response);
    })
  }
}
