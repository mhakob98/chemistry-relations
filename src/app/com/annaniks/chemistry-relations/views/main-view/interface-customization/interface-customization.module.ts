import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { InterfaceCustomizationView } from './interface-customization-view';
import { InterfaceCustomizationRoutingModule } from './interface-customization.routing.module';
import { FilesUploadComponent } from './files-upload/files-upload.component';
import { InterfaceCustomizationService } from './interface-customization.service';
import { TextsAndColorsComponent } from './texts-and-colors/texts-and-colors.component';
import { AddFieldModal } from '../../../modals/add-field-modal/add-field-modal';
import { MaterialModule } from '../../../shared/material.module';

@NgModule({
    declarations: [
        InterfaceCustomizationView,
        FilesUploadComponent,
        TextsAndColorsComponent,
        AddFieldModal
    ],
    imports: [
     CommonModule,
     SharedModule,
     InterfaceCustomizationRoutingModule,
     MaterialModule
    ],
    providers: [
        InterfaceCustomizationService
    ],
    entryComponents:[AddFieldModal]
})
export class InterfaceCustomizationModule {}