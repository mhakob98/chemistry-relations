import { InterfaceCustomizationView } from './interface-customization-view';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let interfaceCustomizationViewRoutes: Routes = [
    {
        path: '', component: InterfaceCustomizationView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(interfaceCustomizationViewRoutes)],
    exports: [RouterModule]
})
export class InterfaceCustomizationRoutingModule { }