import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { InterfaceCustomizationService } from "../interface-customization.service";
import { httpParams } from "../../../../params/httpParams";

@Component({
  selector: "texts-and-colors",
  templateUrl: "./texts-and-colors.component.html",
  styleUrls: ["./texts-and-colors.component.scss"]
})
export class TextsAndColorsComponent implements OnInit {
  @Input() label: string = "Интерфейс";
  @Input() key: string = "interface";
  @Input() columnCount: number = 2;
  public baseUrl: string = httpParams.photoBaseUrl;

  public showLoadingOverlay: boolean = false;
  private _allFieldOfGroup: any;

  constructor(
    private _interfaceCustomiaztionService: InterfaceCustomizationService
  ) {}

  ngOnInit() {
    console.log(this.key);

    this.getAllFieldsOfGroup();
  }
  private getAllFieldsOfGroup(): void {
    console.log(this.key);

    if (this.key == "operations") {
      this._interfaceCustomiaztionService
        .getSettings()
        .subscribe((settings: any) => {
          this._allFieldOfGroup = settings[0];
        });
    } else {
      this._interfaceCustomiaztionService
        .getAllFieldsOfGroup(this.key)
        .subscribe(response => {
          this._allFieldOfGroup = response;
        });
    }
  }
  private _updateFieldsOfGroup(): void {
    this.showLoadingOverlay = true;
    if (this.key == "operations") {
      console.log(this._allFieldOfGroup);
      this._interfaceCustomiaztionService
        .updateSettings(this._allFieldOfGroup)
        .subscribe(settings => {
          this.getAllFieldsOfGroup();
          this.showLoadingOverlay = false;
        });
    } else {
      this._interfaceCustomiaztionService
        .updateFieldsOfGroup(this._allFieldOfGroup.data, this.key)
        .subscribe(response => {
          this.getAllFieldsOfGroup();
          this.showLoadingOverlay = false;
        });
    }
  }
  get allFieldOfGroup(): any {
    return this._allFieldOfGroup;
  }
  public uploadImage(event, id: string): void {
    let formData = new FormData();
    if (event && event.target) {
      let fileList: FileList = event.target.files;
      if (fileList.length > 0) {
        let file: File = fileList[0];
        formData.append("file", file, file.name);
      }
    }
    this._upload(formData, this.key, id);
  }
  private _upload(formData: FormData, key: string, id: string): void {
    this._interfaceCustomiaztionService
      .uploadImageField(formData, key, id)
      .subscribe(response => {
        console.log(response);
      });
  }
  public delteField(field): void {
    let index = this._allFieldOfGroup.data.indexOf(field);
    if (index > -1) {
      this._allFieldOfGroup.data.splice(index, 1);
    }
  }
}
