import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClientsRoutingModule } from './clients-routing.module';
import { ClientsAddFormComponent } from './clients-add-form/clients-add-form.component';
import { ClientsComponent } from './clients.component';
import { ClientsService } from './clients.service';

@NgModule({
  declarations: [ClientsAddFormComponent, ClientsComponent],
  imports: [
    CommonModule,
    ClientsRoutingModule,
    SharedModule
  ],
  providers:[
    ClientsService
  ]
})
export class ClientsModule { }
