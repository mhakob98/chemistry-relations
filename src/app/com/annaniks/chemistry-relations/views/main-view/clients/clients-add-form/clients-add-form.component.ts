import { Component, OnInit, Output,EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ClientsService } from './../clients.service';

@Component({
  selector: 'clients-add-form',
  templateUrl: './clients-add-form.component.html',
  styleUrls: ['./clients-add-form.component.scss']
})
export class ClientsAddFormComponent implements OnInit {
  private _clientsForm:FormGroup
  private _percents:any
  @Output() clientsAdded:EventEmitter<void> = new EventEmitter<void>()
  constructor(
    private _fb:FormBuilder,
    private _clientsService:ClientsService
  ) { }

  ngOnInit() {
    this._formBuilder();

  }
  private _formBuilder():void{
    this._clientsForm = this._fb.group({
      smallClient:'',
      averageClient:'',
      bigClient:'',
      veryBigClient:''
    })
    this._getPercents() 

  }
  public addClients():void{
    this._clientsService.addClients(this._clientsForm.value).subscribe(response=>{
      console.log(response);
      this.clientsAdded.emit()
    })
  }
  public resetValues():void{
    this._clientsForm.setValue({
      smallClient:'',
      averageClient:'',
      bigClient:'',
      veryBigClient:''
    })  
  }
  private _getPercents():void{
    this._clientsService.getPercents().subscribe(response=>{
      this._percents = response
      console.log(this.percents);
      
    })
  }
  get clientsForm():FormGroup{
    return this._clientsForm
  }
  get percents():any{
    return this._percents;
  }
}
