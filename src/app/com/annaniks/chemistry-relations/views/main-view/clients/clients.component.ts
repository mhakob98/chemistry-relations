import { CityResponse } from './../../../models/models';
import { Component, OnInit } from '@angular/core';
import { ClientsService } from './clients.service';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss']
})
export class ClientsComponent implements OnInit {
  private _allCities:CityResponse;
  private _showLoadingOverlay:boolean = false;
  constructor(
    private _clientsService:ClientsService
  ) { }

  ngOnInit() {
    this._getAllCities();
  }
  public handleClientsAdded(clients:any){
    this._getAllCities();
  }
  private _getAllCities():void{
    this._showLoadingOverlay = true;
    this._clientsService.getCities().subscribe((allCities:CityResponse) =>{
      console.log(allCities);
      this._allCities = allCities
      this._showLoadingOverlay = false;
    })
  }
 
  get allCities():CityResponse{
    return this._allCities;
  }
  get showLoadingOverlay():boolean{
    return this._showLoadingOverlay;
  }
}
