import { ClientsAddFormComponent } from './clients-add-form/clients-add-form.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientsComponent } from './clients.component';

const clientsViewRoutes: Routes = [
  { path:'',component:ClientsComponent,children:[

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(clientsViewRoutes)],
  exports: [RouterModule]
})
export class ClientsRoutingModule { }
