import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { ApiService } from './../../../services/api.service';
import { CityResponse } from '../../../models/models';

@Injectable()
export class ClientsService{
    constructor(private _apiService:ApiService){ }

    public addClients(clientsForm:any):Observable<any>{
        return this._apiService.post("city/client",clientsForm)
    }
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
    public getPercents():Observable<any>{
        return this._apiService.get("/client/config")
    }
}