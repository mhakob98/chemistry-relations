import { BotsComponent } from './bots';
import { SharedModule } from './../../../shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../../../shared/material.module';
import { BotsRoutingModule } from './bots.routing.module';
import { BotsControl } from '../../../modals/bots-control/bots-control';
import { EachCityBotsComponent } from './each-city-bots/each-city-bots.component';
import { BotsService } from './bots.service';


@NgModule({
  declarations: [BotsComponent,BotsControl, EachCityBotsComponent],
  imports: [
    CommonModule,
    SharedModule,
    MaterialModule,
    BotsRoutingModule
  ],
  providers:[BotsService],
  entryComponents:[BotsControl]
})
export class BotsModule { }
