import { ApiService } from './../../../services/api.service';
import { Injectable } from '@angular/core';
import { CityResponse } from '../../../models/models';
import { Observable } from 'rxjs';
@Injectable()
export class BotsService{
    constructor(private _apiService:ApiService){}
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
    public getAllBots():Observable<any>{
        return this._apiService.get("/bot")
    }
}