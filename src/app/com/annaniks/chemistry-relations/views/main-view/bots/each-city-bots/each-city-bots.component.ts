import { Socket } from 'ngx-socket-io';
import { Component, OnInit, Input } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
  selector: '[each-city-bots]',
  templateUrl: './each-city-bots.component.html',
  styleUrls: ['./each-city-bots.component.scss']
})
export class EachCityBotsComponent implements OnInit {
  @Input() cityBots;
  @Input() addCorner:boolean = true;
  @Input() isLast:boolean = false;
  public easy = []
  public medium= []
  public hard =  []
  public positons:string[] = [];
  public showLoadingOverlay:boolean = false;
  constructor(private _socket:Socket) { }
  ngOnInit() {
    this.botMatrix();
    this._socket.on("bot-pause-type",botGroupPaused=>{
      console.log(botGroupPaused);
      
    })
    this._socket.on("bot-start-type",botGroupPaused=>{
      console.log(botGroupPaused);
      
    })
    this._socket.on("bot-switch-type",botGroupPaused=>{
      console.log(botGroupPaused);
      
    })
    
    this._socket.once('bot-matrix',response=>{
      this.positons = response;
      this._setBotsPositions(this.positons)
    })
  }
  ngAfterViewInit(): void {
  }

  public switchType(type,newType):void{      
    this._socket.emit("bot-switch-type",{ type, newType, cityId:this.cityBots._id})
  }
  public stopBotsGroup(type):void{        
    this._socket.emit("bot-pause-type",{cityId:this.cityBots._id,type:type})
    // this._changeShowOverlay();
  }
  public startBotsGame(type):void{
    this._socket.emit("bot-start-type",{cityId:this.cityBots._id,type:type})
    // this._changeShowOverlay();
  }

  public botMatrix():void{

    this._socket.emit("bot-matrix",{cityId:this.cityBots._id})
  }

  private _setBotsPositions(botpositions:string[]):void{
    console.log(botpositions,"botpositionsbotpositionsbotpositionsbotpositionsbotpositionsbotpositionsbotpositionsbotpositionsbotpositionsbotpositions");
    
    botpositions.forEach((element,index)=>{
      if(+element==0){
        this.easy.push(+index);
      }
      if(+element==1){
        this.medium.push(+index)
      }
      if(+element==2){
        this.hard.push(+index)
      }
    })
  }

  drop(event: CdkDragDrop<string[]>) {   
    
    if (event.previousContainer == event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        0);
    }   
    // this._changeShowOverlay();

      this.switchType(event.container.data[0].toString(),event.container.element.nativeElement.classList[1])

  }

  private _changeShowOverlay():void{
    this.showLoadingOverlay = true;
    setTimeout(()=>{
      this.showLoadingOverlay = false;
    },1000) 
  }
  ngOnDestroy(): void {
    this._socket.removeListener("bot-pause-type");
    this._socket.removeListener("bot-start-type");
    this._socket.removeListener("bot-switch-type");
    this._socket.removeListener("bot-matrix");
  }
}
