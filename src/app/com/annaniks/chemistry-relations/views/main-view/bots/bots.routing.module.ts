import { BotsComponent } from './bots';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const botsViewRoutes: Routes = [
  { path:'',component:BotsComponent,children:[

  ]}
];

@NgModule({
  imports: [RouterModule.forChild(botsViewRoutes)],
  exports: [RouterModule]
})
export class BotsRoutingModule { }
