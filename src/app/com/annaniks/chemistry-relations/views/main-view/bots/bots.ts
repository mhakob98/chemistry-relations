import { Socket } from 'ngx-socket-io';
import { BotsService } from './bots.service';
import { CityResponse } from './../../../models/models';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BotsControl } from '../../../modals/bots-control/bots-control';

@Component({
  templateUrl: './bots.html',
  styleUrls: ['./bots.scss']
})
export class BotsComponent implements OnInit {
  private _allCities:CityResponse;
  public showLoadingOverlay:boolean = false;
  public isActiveBot:boolean = false;
  constructor(private _dialog: MatDialog,private _botsService:BotsService,private _socket:Socket) { }

  ngOnInit() {
 
    this._getAllCities()
    this._checkIfGameStarted();
    this._socket.on("bot-state",botsStoped =>{
      console.log("QWEWQEQWEQWEQWEQWEWEQWE",botsStoped);
      
        botsStoped.initialized ? this.isActiveBot = true : this.isActiveBot = false;
    })
    this._socket.on("bot-start-all",botsStarted => {
      this.isActiveBot = true;   
    })
    this._socket.on("bot-pause-all",botsStoped =>{
   
    })
  }
 
  public openBotsControl(): void {
    const dialogRef = this._dialog.open(BotsControl, {
      width: '485px',
      panelClass:'bots_action',
      data:this.allCities
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
      
    });
  }
  public startAllBots():void{
    this._socket.emit("bot-start-all");
    // this._changeShowOverlay();
  }
  public stopAllBots():void{
    this._socket.emit("bot-pause-all")
    // this._changeShowOverlay()
  }
  private _checkIfGameStarted():void{
    this._socket.emit("bot-state");
  }
  private _getAllCities():void{
    this._botsService.getCities().subscribe((cities:CityResponse)=>{
      this._allCities = cities
      console.log(this._allCities);
      
    })
  }
  private _changeShowOverlay():void{
    this.showLoadingOverlay = true;
    setTimeout(()=>{
      this.showLoadingOverlay = false;
    },5000) 
  }

  get allCities():CityResponse{
    return this._allCities
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this._socket.removeListener("bot-start-all");
    this._socket.removeListener("bot-pause-all");
    this._socket.removeListener("bot-state");
  }
}
