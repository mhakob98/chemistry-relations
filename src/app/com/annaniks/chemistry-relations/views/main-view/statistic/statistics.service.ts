import { Observable } from 'rxjs';
import { Injectable } from "@angular/core";
import { ApiService } from './../../../services/api.service';

@Injectable()
export class StatisticsService{
    constructor(private _apiService:ApiService){

    }
    public getSupportOneData():Observable<any>{
        return this._apiService.get("crizis/getAllSupportOne");
    }
    public getSupportTwoData():Observable<any>{
        return this._apiService.get("crizis/getAllSupportTwo")
    }
    public getCrysisData():Observable<any>{
        return this._apiService.get("crizis/getAllCrizis")
    }
    public getBigCrysisData():Observable<any>{
        return this._apiService.get("crizis/getAllBigCrizis")
    }
}