import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Statistic } from './statistic';

let statisticRoutes: Routes = [
    {
        path: '', component: Statistic, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(statisticRoutes)],
    exports: [RouterModule]
})
export class StatisticRoutingModule { }