import { StatisticsService } from "./statistics.service";
import { Component, OnInit } from "@angular/core";
const STATISTICTYPES: Array<any> = [
  {
    key: "supportOne",
    value: "Поддержка №1"
  },
  {
    key: "supportTwo",
    value: "Поддержка №2"
  },
  {
    key: "crysis",
    value: "Кризис"
  },
  {
    key: "bigCrysis",
    value: "Мировой кризис"
  }
];

@Component({
  selector: "statistic",
  templateUrl: "./statistic.html",
  styleUrls: ["./statistic.scss"]
})
export class Statistic implements OnInit {
  private _statisticTypes: Array<any> = STATISTICTYPES;
  private _activeType: any = this._statisticTypes[0];
  private _supportOneData: any = [];
  private _supportTwoData: any = [];
  private _crysisData: any = [];
  private _bigCrysisData: any = [];
  private _addedChannelsBySupportOne =[];
  private _loseChannelByCrysis = [];
  private _bigCrysisDeletedChannels = [];
  private _firstCrysisResult = [];
  private _lastCrysisResult = [];
  constructor(private _statisticsService: StatisticsService) {}

  ngOnInit() {
    this._getSupportOneData();
    this._getSupportTwoData();
    this._getCrysisData();
    this._getBigCrysisData()
  }
  public setActiveType(type: string): void {
    this._activeType = type;
  }
  private _getSupportOneData(): void {
    this._statisticsService.getSupportOneData().subscribe(supportOneData => {
      console.log("support one is ", supportOneData);
      for (let element of supportOneData) {
        for (let entryElement of element.SupportOne)
          if (!entryElement.channels) {
            this._supportOneData.push(entryElement);
          } else {
            this._addedChannelsBySupportOne.push(entryElement);
          }
      }
    });
  }
  private _getSupportTwoData(): void {
    this._statisticsService.getSupportTwoData().subscribe(supportTwoData => {
      for (let element of supportTwoData) {
        for (let entryElement of element.SupportTwo)
          this._supportTwoData.push(entryElement);
      }
    });
  }
  private _getCrysisData(): void {
    this._statisticsService.getCrysisData().subscribe(crysisData => {
      console.log(crysisData);

      for (let entry of crysisData) {
        for (let entryElement of entry.Crizis) {
          this.goDeepInData(entryElement.result, this._loseChannelByCrysis);
          this.goDeepInData(entryElement.result1, this._crysisData);
        }
      }
    });
  }
  private _getBigCrysisData(): void {
    this._statisticsService.getBigCrysisData().subscribe(bigCrysisData=>{
      let resultCount;
      for (let entry of bigCrysisData) {
        for (let entryElement of entry.BigCrizis) {
          this.goDeepInData(entryElement.result, this.firstCrysisResult);
          this.goDeepInData(entryElement.result1, this._bigCrysisData);
          this.goDeepInData(entryElement.result2, this._bigCrysisData)
          this.goDeepInData(entryElement.result3, this._bigCrysisData)
          this.goDeepInData(entryElement.result4, this.lastCrysisResult);
        }
      }
    })
  }
  public goDeepInData(item, array): void {
    for (let entry of item) {
      if (!entry.deleteDpartner) {
        array.push(entry);
      } else {
        this._bigCrysisDeletedChannels.push(entry);
      }
    }
  }
  get statisticTypes(): Array<any> {
    return this._statisticTypes;
  }
  get activeType(): string {
    return this._activeType;
  }
  get supportOneData(): any {
    return this._supportOneData;
  }
  get addedChannelsBySupportOne(): any {
    return this._addedChannelsBySupportOne;
  }
  get supportTwoData(): any {
    return this._supportTwoData;
  }
  get crysisData(): any {
    return this._crysisData;
  }
  get loseChannelByCrysis(): any {
    return this._loseChannelByCrysis;
  }
  get bigCrysisData(): any {
    return this._bigCrysisData;
  }
  get bigCrysisDeletedChannels():any{
    return this._bigCrysisDeletedChannels
  }
  get firstCrysisResult():any{
    return this._firstCrysisResult
  }
  get lastCrysisResult():any{
    return this._lastCrysisResult;
  }
}
