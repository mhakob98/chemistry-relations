import { StatisticRoutingModule } from './statistic.routing.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { Statistic } from './statistic';
import { StatisticsService } from './statistics.service';
@NgModule({
    declarations: [
        Statistic
    ],
    imports: [
     CommonModule,
     SharedModule,
     StatisticRoutingModule
    ],
    providers: [
        StatisticsService
    ],
    entryComponents:[]
})
export class StatisticModule {}