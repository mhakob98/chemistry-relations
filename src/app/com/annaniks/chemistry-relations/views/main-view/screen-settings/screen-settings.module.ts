import { ScreenSettingsView } from './screen-setting-view';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../../shared/shared.module';
import { ScreenSettingsRoutingModule } from './screen-setting.routing.module';
import { ScreenSettingsService } from './screen-settings.service';

@NgModule({
    declarations: [
       ScreenSettingsView
    ],
    imports: [
     CommonModule,
     ScreenSettingsRoutingModule,
     SharedModule
    ],
    providers: [
        ScreenSettingsService
    ],
    entryComponents:[]
})
export class ScreenSettingsModule {}