import { ScreenSettingsView } from './screen-setting-view';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

let screenSettingsViewRoutes: Routes = [
    {
        path: '', component: ScreenSettingsView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(screenSettingsViewRoutes)],
    exports: [RouterModule]
})
export class ScreenSettingsRoutingModule { }