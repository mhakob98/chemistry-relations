import { ApiService } from './../../../services/api.service';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { CityResponse } from '../../../models/models';

@Injectable()
export class ScreenSettingsService{
    constructor(private _apiService:ApiService){}
    public getCities():Observable<CityResponse>{
        return this._apiService.get("/city");
    }
}