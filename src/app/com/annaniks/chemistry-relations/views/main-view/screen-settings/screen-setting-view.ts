import { CityResponse } from './../../../models/models';
import { ScreenSettingsService } from './screen-settings.service';
import { Component, OnInit } from '@angular/core';
const SCREENSIZES:Array<string> = ["Экран рейтингов","Гл. Экран №1","Гл. Экран №2","Гл. Экран №3","Гл. Экран №4",]
@Component({
  selector: 'app-screen-settings',
  templateUrl: './screen-settings-view.html',
  styleUrls: ['./screen-settings-view.scss']
})
export class ScreenSettingsView implements OnInit {
  private _screenSizes:Array<string> = SCREENSIZES;
  private _allCities:CityResponse;
  constructor(private _screenSettingsService:ScreenSettingsService) { }

  ngOnInit() {
    this._getAllCities()
  }
  private _getAllCities():void{
    this._screenSettingsService.getCities().subscribe((cities:CityResponse)=>{
      this._allCities = cities;
      console.log(this._allCities);
      
    })
  }
  get screenSizes():Array<string>{
    return this._screenSizes;
  }
  get allCities():CityResponse{
    return this._allCities;
  }
}
