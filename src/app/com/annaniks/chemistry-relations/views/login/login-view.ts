import { CookieService } from 'angular2-cookie/core';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { LoginResponse } from '../../models/models';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login-view.html',
  styleUrls: ['./login-view.scss']
})
export class LoginView implements OnInit {
  private _loginForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _loginService:LoginService,
    private _cookieService:CookieService,
    private _router:Router
  ) { }

  ngOnInit() {
    this._formBuilder()
  }
  private _formBuilder() {
    this._loginForm = this._fb.group({
      login: ["", [Validators.required,Validators.minLength(5)]],
      password: ["", [Validators.required,Validators.minLength(4),]],
    });
  }
  private _signIn():void{
    this._loginService.signIn(this._loginForm.value).subscribe((loginResponse:LoginResponse)=>{
      this._cookieService.put("Client_accessToken",loginResponse.token);
      this._router.navigate([''])
    })
  }
  public onSignIn():void{
    this._loginForm.valid ? this._signIn() : null;
  }
  
  get loginForm():FormGroup{
    return this._loginForm;
  }
 
}
