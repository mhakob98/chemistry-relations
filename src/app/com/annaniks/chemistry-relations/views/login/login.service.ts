import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { ApiService } from './../../services/api.service';
import { LoginInterface } from '../../models/models';
@Injectable()
export class LoginService{
    constructor(private _apiService:ApiService){}
    public signIn(signInInfo:LoginInterface):Observable<any>{
        return this._apiService.post("login",signInInfo)
    }
}
