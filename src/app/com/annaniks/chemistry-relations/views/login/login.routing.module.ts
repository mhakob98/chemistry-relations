import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginView } from './login-view';

let loginViewRoutes: Routes = [
    {
        path: '', component: LoginView, children: [
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(loginViewRoutes)],
    exports: [RouterModule]
})
export class LoginRoutingModule { }