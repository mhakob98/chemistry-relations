import { LoginView } from './login-view';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../shared/shared.module';
import { LoginRoutingModule } from './login.routing.module';
import { LoginService } from './login.service';

@NgModule({
    declarations: [
       LoginView
    ],
    imports: [
     CommonModule,
     SharedModule,
     LoginRoutingModule
    ],
    providers: [
        LoginService
    ],
})
export class LoginViewModule {}