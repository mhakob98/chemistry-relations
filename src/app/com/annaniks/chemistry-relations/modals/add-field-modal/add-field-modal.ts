import { InterfaceCustomizationService } from "./../../views/main-view/interface-customization/interface-customization.service";
import { Component, OnInit, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: "app-add-field-modal",
  templateUrl: "./add-field-modal.html",
  styleUrls: ["./add-field-modal.scss"]
})
export class AddFieldModal implements OnInit {
  private _fieldAddForm: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<AddFieldModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private _interfaceCustomizationService: InterfaceCustomizationService
  ) {}

  ngOnInit() {
    this._formBuilder();
  }
  private _formBuilder(): void {
    this._fieldAddForm = this.fb.group({
      key: ["", [Validators.required]],
      ruKey:["",[Validators.required]],
      groupOfFields:["",[Validators.required]],
      isImage:[false]
    });
    this._fieldAddForm.valueChanges.subscribe(data=>{
      console.log(data);
      
    })
  }
  public onFieldAdd():void{
    this._fieldAddForm.valid ? this._addField() : null;
  }
  private _addField():void{
      this._interfaceCustomizationService.addField(this._fieldAddForm.value).subscribe(response=>{
        this.dialogRef.close({added:true})
      })
  }
  get fieldAddForm(): FormGroup {
    return this._fieldAddForm;
  }
}
