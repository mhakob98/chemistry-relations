import { CitiesService } from './../../views/main-view/cities-view/cities.service';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CityResponse } from '../../models/models';

@Component({
  templateUrl: './add-city-modal.html',
  styleUrls: ['./add-city-modal.scss']
})
export class AddCityModal implements OnInit {
  private _cityAddForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddCityModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private _citiesService:CitiesService
  ) {}

  ngOnInit() {
    this._formBuilder();
  }
  private _formBuilder() {
    this._cityAddForm = this.fb.group({
      name: ["", [Validators.required]],
    });
  }
  public onAddCity():void{
    this._cityAddForm.valid ? this._addCity(): null;
  }
  private _addCity():void{
      this._citiesService.addCity(this._cityAddForm.value["name"]).subscribe((response:CityResponse)=>{
        this.dialogRef.close({added:true});
      })
  }
  get cityAddForm():FormGroup{
    return this._cityAddForm;
  }
}
