import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TeamsService } from '../../views/main-view/teams-view/teams.service';
import { CityResponse } from '../../models/models';

@Component({
  templateUrl: './add-team-modal.html',
  styleUrls: ['./add-team-modal.scss']
})
export class AddTeamModal implements OnInit {
  private _teamAddForm: FormGroup;
  private _allCities:CityResponse;

  constructor(
    public dialogRef: MatDialogRef<AddTeamModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _fb: FormBuilder,
    private _teamsService:TeamsService
  ) { }


  ngOnInit() {
    this._getCities()
  }
  private _formBuilder() {
    this._teamAddForm = this._fb.group({
      count: [""],
      cityId:[this._allCities.data[0]._id,[Validators.required]],
      isConcurent:[false,[Validators.required]],
      name:[null],
      code:[null]
    });
    // this._teamAddForm.valueChanges.subscribe(data=>{console.log(data);
    // })
  }
  public onTeamAdd():void{
    // this._teamAddForm.valid ? this._addTeam(): null;
    this._addTeam()
  }
  private _addTeam():void{
    if(this._teamAddForm.value.code !== null){
      this._teamsService.addSingleTeam(this._teamAddForm.value).subscribe(response=>{
        this.dialogRef.close({added:true})
        
      })
    } else
    this._teamsService.addTeam(this._teamAddForm.value).subscribe(response=>{
      this.dialogRef.close({added:true})
      
    })
  }
  private _getCities():void{
    this._teamsService.getCities().subscribe((cities:CityResponse)=>{
      if(cities.message == "ok"){
        this._allCities = cities  
        console.log(cities);
        
        this._formBuilder();

      }
    })
  }
  get teamAddForm():FormGroup{
    return this._teamAddForm;
  }
  get allCities():CityResponse{
    return this._allCities
  }


}
