import { BotsService } from './../../views/main-view/bots/bots.service';
import { Socket } from 'ngx-socket-io';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'bots-control',
  templateUrl: './bots-control.html',
  styleUrls: ['./bots-control.scss']
})
export class BotsControl implements OnInit {
  private _allBots:any;
  constructor(
    public dialogRef: MatDialogRef<BotsControl>,
    @Inject(MAT_DIALOG_DATA) public data,
    private _socket:Socket,
    private _botsService:BotsService
  ) { }

  ngOnInit() {
    this.getAllBots()
    this._socket.on("bot-start-current",singleBotStarted=>{
      console.log(singleBotStarted);
      this.getAllBots()

    })
    this._socket.on("bot-pause-current",singleBotStoped=>{
      console.log(singleBotStoped);
      this.getAllBots()
    })
   
  }
  public returnNameById(id){
    for (const iterator of this.data.data) {
      if(iterator._id == id){
        return iterator.name
      }
    }
  
  }
  public getAllBots(){
    this._botsService.getAllBots().subscribe(allBots=>{
      this._allBots = allBots
      console.log(this.allBots);
      this.allBots.forEach(element => {
          element.cityName = this.returnNameById(element.city);
      });
      
    })
  }
  public startSingleBot(type,_id,cityId):void{
    console.log("gnacccc");
    
    this._socket.emit("bot-start-current",{type,_id,cityId})
    setTimeout(() => {
      this.getAllBots()
    });

  }
  public stopSingleBot(type,_id,cityId):void{
    console.log("gnacccc");
    
    this._socket.emit("bot-pause-current",{type,_id,cityId})
    setTimeout(() => {
      this.getAllBots()
    });
  }
  public closeDialog():void{    
    this.dialogRef.close();
  }

  get allBots():any{
    return this._allBots
  }
  ngOnDestroy(): void {
    this._socket.removeListener("bot-start-current");
    this._socket.removeListener("bot-pause-current");
  }
}
