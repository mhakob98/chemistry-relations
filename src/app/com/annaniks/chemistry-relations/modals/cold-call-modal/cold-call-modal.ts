import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  templateUrl: './cold-call-modal.html',
  styleUrls: ['./cold-call-modal.scss']
})
export class ColdCallModal implements OnInit {
  private _callAddForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<ColdCallModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,

  ) { }

  ngOnInit() {
    this._formBuilder();
  }
  private _formBuilder() {
    this._callAddForm = this.fb.group({
      coldCallsLength: ["", [Validators.required]],
      energiesColdCalls: ["", [Validators.required]]
    });
    this._callAddForm.valueChanges.subscribe(data=>{
      console.log(data);
      
    })
  }

  public closeDialog():void{
    this.dialogRef.close(this.callAddForm.value)
  }
  get callAddForm():FormGroup{
    return this._callAddForm;
  }
}
