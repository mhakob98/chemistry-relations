import { RemoteControlService } from './../../views/main-view/remote-control/remote-control.service';
import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-add-clients-game-time',
  templateUrl: './add-clients-game-time-modal.html',
  styleUrls: ['./add-clients-game-time-modal.scss']
})
export class AddClientsGameTimeModal implements OnInit {
  private _clientdGameTimeAddForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<AddClientsGameTimeModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private _remoteControlService:RemoteControlService
  ) {}

  ngOnInit() {
    this._formBuilder();
  }
  private _formBuilder() {
    this._clientdGameTimeAddForm = this.fb.group({
      count: [10, [Validators.required]],
    });
  }
  public onAddClient(){
    this._clientdGameTimeAddForm.valid ? this._addClient() : null
  }
  private _addClient():void{
    this._remoteControlService.addClients(this._clientdGameTimeAddForm.value.count).subscribe((response)=>{
      this.dialogRef.close({added:true});
    },error=>{
      this.dialogRef.close({added:false})
    })
  }
  get clientdGameTimeAddForm():FormGroup{
    return this._clientdGameTimeAddForm
  }
}
