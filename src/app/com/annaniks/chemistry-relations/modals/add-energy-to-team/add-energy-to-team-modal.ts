import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RemoteControlService } from '../../views/main-view/remote-control/remote-control.service';
import { Socket } from 'ngx-socket-io';

@Component({
  templateUrl: './add-energy-to-team-modal.html',
  styleUrls: ['./add-energy-to-team-modal.scss']
})
export class AddEnergyToTeamModal implements OnInit {

  private _addEnergyForm: FormGroup;
  private _allTeams:any
  constructor(
    public dialogRef: MatDialogRef<AddEnergyToTeamModal>,
    @Inject(MAT_DIALOG_DATA) public data,
    private fb: FormBuilder,
    private _remoteControlService:RemoteControlService,
    private _socket:Socket
    ) {}

  ngOnInit() {
    this._getAllTeams();
    this._formBuilder();
    this._socket.on('inc-energies',data=>{
      this.dialogRef.close();
    })
  }
  private _formBuilder() {
    this._addEnergyForm = this.fb.group({
      teamName: ["", [Validators.required]],
      energies:["",[Validators.required]]
    });
  }
  public onAddEnergy():void{
    this._addEnergyForm.valid ? this._addEnergy(): null;
  }
  private _addEnergy():void{    
    this._socket.emit('inc-energies',{id:this._addEnergyForm.value.teamName,energies:this._addEnergyForm.value.energies,groupName:this._findNameById(this._addEnergyForm.value.teamName)})
  }
  private _getAllTeams():void{
    this._remoteControlService.getAllTeams().subscribe(allTeams=>{
      this._allTeams = allTeams
    })
  }
  private _findNameById(Id:string):string{
    for(let item of this.allTeams.result ){
      if(item["_id"] == Id ){
        return item["name"]
      }
    }
  }  
  get addEnergyForm():FormGroup{
    return this._addEnergyForm
  }
  get allTeams():any{
    return this._allTeams;
  }
  ngOnDestroy(){
    this._socket.removeListener("inc-energies");
  }
}
