import { CookieService } from 'angular2-cookie/services/cookies.service';
import { MenuItemsService } from './../../services/menu-items.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ScrollService } from './../../services/scroll.service';
import { trigger, style, animate, transition } from '@angular/animations';
const EXPANDKEYS:Array<object> = [
  {
    key:"rounds",
    value:"Раунды"
  },
  {
    key:"mainCoefficient",
    value:"Основные коэффициенты"
  },
  {
    key:"needs",
    value:"Потребность"
  },
  {
    key:"channelImprove",
    value:"Улучшение связей"
  },
  {
    key:"activeLevel",
    value:"Уровень активности"
  },
  {
    key:"activeLevel",
    value:"Уровень проактивности"
  },
  {
    key:"smallCoefficient",
    value:"Коэфф. малого клиента"
  },
  {
    key:"middleCoefficient",
    value:"Коэфф. среднего клиента"
  },
  {
    key:"bigCoefficient",
    value:"Коэфф. большого клиента"
  },
  {
    key:"veryBigCoefficient",
    value:"Коэфф. гипербольшого клиента"
  }
]
@Component({
  selector: 'side-navigation-menu',
  templateUrl: './side-navigation-menu.component.html',
  styleUrls: ['./side-navigation-menu.component.scss'],
  providers:[MenuItemsService]
  
})
export class SideNavigationMenuComponent implements OnInit {
  @Output('handleOpenMenu') private  _handleOpenMenu = new EventEmitter()
  private _isOpened:boolean = false;
  private _isOpenedTest:boolean = false;
  private _expandKeys:Array<any> = EXPANDKEYS
  constructor(private _menuItemsService:MenuItemsService,private _scrollService:ScrollService,private _cookieService:CookieService) { }
  ngOnInit() {
  }
  ngAfterViewInit(){
  
  }
  public onClickExpand():void{
    this._emitExpandEvent();
  }
  private _emitExpandEvent():void{
    this._handleOpenMenu.emit()
  }
  public toggleExpandableMenu(routerLink:string,logout?:boolean,event?):void{
    
    if (event){
      if(event.srcElement.className == "rotate"){
        event.srcElement.className = "rotate_back"
      } else{
        event.srcElement.className = "rotate"
      }
      
    } else {
      this._isOpened = false;
      this._isOpenedTest = false;
    }

    
    
    
    if(logout){
      this._cookieService.remove('Client_accessToken');
      window.location.reload();
    }
    if(routerLink == '/configurations'){
      this._isOpenedTest = false;
      this._isOpened =! this._isOpened;
      this._scrollService.activeElementId = "mainCoefficient"
    } else if(routerLink == '/test-configurations') {
      this._isOpened = false;
      this._isOpenedTest =! this._isOpenedTest
      this._scrollService.activeElementId = "mainCoefficient"
    }
  }
  public scrollToElement(elementId:string):void{
    this._scrollService.triggerScrollTo(elementId);
  }
  get menuItemsService():MenuItemsService{
    return this._menuItemsService;
  }
  get isOpened():boolean{
    return this._isOpened;
  }
  get isOpenedTest():boolean{
    return this._isOpenedTest;
  }
  get expandKeys():Array<any>{
    return this._expandKeys
  }
}
