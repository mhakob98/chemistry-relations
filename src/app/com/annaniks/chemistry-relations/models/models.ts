export interface MenuItem{
    label:string;
    expandable:boolean;
    routerLink:string,
    logout?:boolean
}
export interface ImageUpload{
    label:string;
    key:string
}
export interface TranslateKeys{
    key:string,
    value:string,
}
export interface LoginInterface{
    login:string,
    password:string
}
export interface LoginResponse{
    token:string
}
export interface CityResponse{
    message:string,
    data:Array<{
        averageClient:number,
        bigClient:number,
        name:string,
        smallClient:number,
        veryBigClient:number,
        __v:number,
        _id:string,
    }>
}
export interface RequestParams {
    headers?
    observe?
    responseType?
}
