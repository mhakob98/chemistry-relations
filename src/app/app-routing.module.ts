import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './com/annaniks/chemistry-relations/services/authguard.service';

const appRoutes: Routes = [
  { path: '', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/main-view/main-view.module#MainViewModule',canActivate:[]},
  { path: 'sign-in', loadChildren: 'src/app/com/annaniks/chemistry-relations/views/login/login.module#LoginViewModule' },

];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
